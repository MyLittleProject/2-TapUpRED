package com.aprisunnea.tapup;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainActivity extends Activity implements View.OnClickListener {
    // every View-value variable starts with "player" word and every View component doesn't.
    private int playerBestScore;
    private int playerLastScore;
    private int playerScore = 0;
    private int playerLife;
    private final String[] colors = {"Red", "Blue", "Green", "Yellow", "Pink"};
    private Button[] buttons;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static CountDownTimer timer;
    private boolean timerIsWorking;

    private ProgressBar timeBar;
    private TextView nowScore;
    private TextView bestScore;
    private TextView lastScore;
    private TextView life;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init
        playerLife = 3;
        timeBar = findViewById(R.id.timeBar);
        nowScore = findViewById(R.id.nowScore);
        bestScore = findViewById(R.id.bestScore);
        lastScore = findViewById(R.id.lastScore);
        life = findViewById(R.id.life);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        buttons = new Button[]{btn1, btn2, btn3, btn4, btn5};

        // for the saved scores
        preferences = getSharedPreferences("scorePreferences", Context.MODE_PRIVATE);
        editor = preferences.edit();
        playerBestScore = preferences.getInt("score", 0);
        playerLastScore = preferences.getInt("lastScore", 0);

        // for the timer
        timerIsWorking = false;
        timeBar.setProgress(100);

        nowScore.setText("0");
        life.setText("3");
        bestScore.setText(String.valueOf(playerBestScore));
        lastScore.setText(String.valueOf(playerLastScore));
        reRenderButtons();

        // Listener of each button
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
    }

    private static ArrayList<Integer> generateRandom() {
        ArrayList<Integer> generated = new ArrayList(Arrays.asList(0, 1, 2, 3, 4));
        Collections.shuffle(generated);
        return generated;
    }
    private void reRenderButtons() {
        ArrayList<Integer> randomColor = generateRandom();
        int[] choice = {Color.RED, Color.BLUE, Color.GREEN, Color.MAGENTA, Color.YELLOW};
        for(int i = 0; i < 5; i++) {
            buttons[i].setText(colors[randomColor.get(i)]);
        }
        randomColor = generateRandom();
        for(int i = 0; i < 5; i++) {
            buttons[i].setTextColor(choice[randomColor.get(i)]);
        }
    }
    private void answerTrue() {
        playerScore++;
        nowScore.setText(String.valueOf(playerScore));
        reRenderButtons();
        evaluateScores();

    }
    private void answerFalse() {
        playerLife--;
        if(playerLife == 0) {
            gameEnd("Wrong! the game will restart!\nYour Score: "+ playerScore);
        } else {
            String remainingLife = String.valueOf(playerLife);
            life.setText(remainingLife);
            reRenderButtons();
            Toast.makeText(MainActivity.this,"Wrong! Your remaining life: "+
                    remainingLife, Toast.LENGTH_SHORT).show();
        }
    }
    private void evaluateScores() {

        if (playerScore > playerBestScore) {
            editor.putInt("score", playerScore);
            editor.commit();
            playerBestScore = preferences.getInt("score", 0);
            bestScore.setText(String.valueOf(playerBestScore));
        }

        if(playerLife == 0 || !timerIsWorking) {
            editor.putInt("lastScore", playerScore);
            editor.commit();
            playerLastScore = preferences.getInt("lastScore", 0);
            lastScore.setText(String.valueOf(playerLastScore));
        }
    }
    private void gameEnd(String toastMsg) {
        timerIsWorking = false;
        evaluateScores();
        playerLife = 3;
        timer.cancel();
        playerScore=0;
        nowScore.setText(String.valueOf(playerScore));
        lastScore.setText(String.valueOf(playerLastScore));
        Toast.makeText(MainActivity.this,toastMsg, Toast.LENGTH_LONG).show();
        life.setText(String.valueOf(playerLife));
        reRenderButtons();
        timeBar.setProgress(100);
    }
    private void updateTimer() {
        if(timer != null)
            timer.cancel();
        timerIsWorking = true;
        timer = new CountDownTimer(60000, 100) {
            public void onTick(long millisUntilFinished) {
                timeBar.setProgress((int) millisUntilFinished / 600);
            }

            public void onFinish() {
                gameEnd("Time's up! the game will restart!");
            }
        };
        timer.start();
    }
    @Override
    public void onClick(View v) {
        if(!timerIsWorking)
            updateTimer();
        if(((Button)v).getText().toString().toUpperCase().equals("RED"))
            answerTrue();
        else
            answerFalse();
    }
}



